
### Используемые технологии ###

* DJANGO 2.2
* REST API
* JWT
* VUE-MDB

### Файл requirements.txt ###

```python
Django==2.2
django-filter==2.1.0
djangorestframework==3.9.2
Markdown==3.1
pkg-resources==0.0.0
pytz==2019.1
sqlparse==0.3.0

```
### Пример local_settings.py ###

```python



```
### Отладка ###
```python
import ipdb; ipdb.set_trace()
```
