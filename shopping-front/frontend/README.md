![Моремам](./public/img/icons/favicon-96x96.png "")
# Frontend-версия сайта

## Установка 
```
yarn install
```

### Запуск для разработки
```
yarn run serve
```

### Сборка для продакшена
```
yarn run build
```

### Запуск тестов
```
yarn run test
```

### Запуск линтеров
```
yarn run lint
```

### Информация о компонентах тут
* [mdbootstrap](https://mdbootstrap.com/docs/vue/components/demo/).
