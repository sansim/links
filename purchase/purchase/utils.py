""" any utils """

from django.http.request import HttpRequest


def get_client_ip(request: HttpRequest) -> str:
    """get ip addres from request"""
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    return x_forwarded_for.split(',')[0] if x_forwarded_for else request.META.get('REMOTE_ADDR')
