from django.contrib import admin
from .models import Shopping, Participant, Good, User
# Register your models here.

# class AdminList(admin.ModelAdmin):
#     list_display = ('login', 'first_name')
#     list_filter = ('login',)
#     search_fields = ('login',)

admin.site.register(Shopping)
admin.site.register(Participant)
admin.site.register(Good)

