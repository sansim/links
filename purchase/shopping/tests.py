from django.utils import timezone
from django.test import TestCase
from django.contrib.auth.models import User
from .models import Shopping


class ShoppingTestCase(TestCase):
    def setUp(self):
        manager = User(username='admin', email='a@m.ru')
        manager.save()
        Shopping.objects.create(
            manager=manager, title="test", date_close=timezone.now())

    def test_is_close_w_date(self):
        shopping = Shopping.objects.get(pk=1)
        self.assertTrue(shopping.is_closed())

    def test_is_close(self):
        shopping = Shopping.objects.get(pk=1)
        shopping.is_close = True
        shopping.date_close = None
        shopping.save()
        self.assertTrue(shopping.is_closed())
