import string
import random
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework import generics
from purchase.utils import get_client_ip
from ..models import Participant, Shopping, Good
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticatedOrReadOnly, AllowAny
from ..serializers import (
    ShoppingSerializer,
    ParticipantCreateSerializer,
    ParticipantPrivateSerializer,
    GoodsSerializer,
    ParticipantGoodsSerializer)




class ShoppingParticipantAdd(generics.GenericAPIView):
    queryset = Shopping.objects.all()
    serializer_class = ParticipantCreateSerializer
    lookup_field = 'id'
    permission_classes = (AllowAny,)

    @staticmethod
    def token_gen():
        return ''.join(random.sample((string.ascii_uppercase+string.digits), 6))

    def post(self, request, *args, **kwargs):
        data = {
            "name": request.data.get("name", ""),
            "phone": request.data.get("phone", ""),
            "ip": get_client_ip(request)
        }
        ser = ParticipantCreateSerializer(data=data)
        if not ser.is_valid():
            return Response({"error": "invalid data"}, status=400)
        shopping = self.get_object()
        ser.save()
        participant = ser.instance
        if request.user.is_authenticated:
            participant.user = request.user
        participant.token = self.token_gen()  # TODO: create token
        participant.save()
        shopping.participants.add(participant)
        shopping.save()
        return Response({"token": participant.token})


class ShoppingParticipantEdit(generics.RetrieveUpdateAPIView):
    queryset = Participant.objects.all()
    serializer_class = ParticipantGoodsSerializer
    lookup_field = ('token')
    permission_classes = (AllowAny,)

    def get_object(self):
        pass


class ShoppingParticipantGoodsList(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    serializer_class = GoodsSerializer

    def get_queryset(self):
        return self.get_participant().goods

    def perform_create(self, serializer):
        super(ShoppingParticipantGoodsList, self).perform_create(serializer)
        participant = self.get_participant()
        participant.goods.add(serializer.instance)
        participant.save()

    def get_participant(self) -> Participant:
        token = self.kwargs['token']
        participant = Participant.objects.get(token=token)
        return participant


class ShoppingParticipantGoodsEdit(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    serializer_class = GoodsSerializer
    lookup_field = 'id'

    def get_participant(self) -> Participant:
        token = self.kwargs["token"]
        participant = get_object_or_404(Participant, token=token)
        return participant

    def get_goods(self) -> Good:
        id = self.kwargs["id"]
        participant = self.get_participant()
        goods = participant.goods.filter(id=id)
        return goods

    def get_queryset(self):
        return self.get_goods()
