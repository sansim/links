from django.shortcuts import render, get_object_or_404
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticatedOrReadOnly, AllowAny, IsAdminUser, IsAuthenticated
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework import generics
from ..models import Shopping
from ..serializers import ShoppingSerializer, ShoppingSerializerId, ParticipantPublicSerializer
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from ..permission import ShoppingOwnerPermission
# Create your views here.

class StandardResultsSetPagination(PageNumberPagination):
    page_size = 1000
    page_size_query_param = 'page_size'
    max_page_size = 1000


class ShoppingListView(generics.ListAPIView):
    # authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = Shopping.objects.all().order_by('-date')
    serializer_class = ShoppingSerializer
    pagination_class = StandardResultsSetPagination


class ShoppingDeleteView(generics.DestroyAPIView):
    queryset = Shopping.objects.all()
    serializer_class = ShoppingSerializer
    lookup_field = 'id'
    permission_classes = (IsAdminUser,)


class ShoppingCreateView(generics.CreateAPIView):
    queryset = Shopping.objects.all()
    serializer_class = ShoppingSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        zakupka = ShoppingSerializer(data=request.data)
        if zakupka.is_valid():
            zakupka.save(manager=request.user)
            return Response({'status': 'ADD'})
        else:
            return Response({'status': 'Error'})

class ShoppingIdDetail(viewsets.ModelViewSet):
    queryset = Shopping.objects.all()
    lookup_field = 'id'
    permission_classes = (ShoppingOwnerPermission,)

    def get_serializer_class(self):
        user = self.request.user
        if user == self.get_object().manager:
            serializer_class = ShoppingSerializerId
        else:
            serializer_class = ShoppingSerializer
        return serializer_class

