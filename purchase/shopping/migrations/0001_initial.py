# Generated by Django 2.2 on 2019-05-25 08:20

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Good',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('href', models.CharField(max_length=255, verbose_name='Ссылка на товар')),
                ('color', models.CharField(blank=True, max_length=50, verbose_name='Цвет')),
                ('size', models.CharField(blank=True, max_length=50, verbose_name='Размер')),
                ('articul', models.CharField(blank=True, max_length=50, verbose_name='Артикул')),
                ('quantity', models.PositiveIntegerField(default=1, verbose_name='Кол-во')),
                ('description', models.TextField(blank=True, verbose_name='Описание')),
                ('price', models.PositiveIntegerField(default=0, verbose_name='Цена')),
            ],
            options={
                'verbose_name': 'Товар',
                'verbose_name_plural': 'Товары',
            },
        ),
        migrations.CreateModel(
            name='Participant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('token', models.CharField(max_length=50, verbose_name='token')),
                ('name', models.CharField(max_length=50, verbose_name='ФИО')),
                ('phone', models.CharField(blank=True, max_length=50, verbose_name='Телефон')),
                ('contacts', models.TextField(blank=True, verbose_name='Дополнительные контакты')),
                ('ip', models.GenericIPAddressField(verbose_name='ip')),
                ('goods', models.ManyToManyField(blank=True, related_name='participant', to='shopping.Good')),
            ],
            options={
                'verbose_name': 'Участник',
                'verbose_name_plural': 'Участники',
            },
        ),
        migrations.CreateModel(
            name='Shopping',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50, verbose_name='Название')),
                ('description', models.TextField(blank=True, verbose_name='Описание закупки')),
                ('date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Дата')),
                ('conditions', models.TextField(blank=True, verbose_name='Условия закупки')),
                ('delivery', models.TextField(blank=True, verbose_name='Доставка')),
                ('manager', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('participants', models.ManyToManyField(blank=True, to='shopping.Participant')),
            ],
            options={
                'verbose_name': 'Закупка',
                'verbose_name_plural': 'Закупки',
            },
        ),
    ]
