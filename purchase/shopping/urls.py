from django.urls import path, include
from .views import (
    ShoppingDeleteView,
    ShoppingListView,
    ShoppingCreateView,
    ShoppingIdDetail,
    ShoppingParticipantAdd,
    ShoppingParticipantGoodsEdit,
    ShoppingParticipantGoodsList,
    ShoppingParticipantEdit)
from rest_framework import routers

urlpatterns = [

    path('list/', ShoppingListView.as_view(), name='list'),
    path('<int:id>/delete/', ShoppingDeleteView.as_view(), name='delete'),
    path('create/', ShoppingCreateView.as_view(), name='create'),
    path('<int:id>/participant/', ShoppingParticipantAdd.as_view(), name='participant'),
    path('<int:id>/', ShoppingIdDetail.as_view({'get': 'retrieve', 'put':'partial_update'}), name='shoping-id'),
    path('<str:token>/', ShoppingParticipantEdit.as_view()),
    path('<str:token>/goods/', ShoppingParticipantGoodsList.as_view({'get': 'list', 'post': 'create'})),
    path('<str:token>/goods/<int:id>/', ShoppingParticipantGoodsEdit.as_view({'get': 'retrieve', 'put': 'update', 'delete':'destroy'}))
]
