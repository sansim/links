from rest_framework import permissions

class ShoppingOwnerPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        shopping = view.get_object()
        return request.user == shopping.manager


class ShoppingOwnerPermission1(permissions.BasePermission):

    def has_permission(self, request, view):
        pass