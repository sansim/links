from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Shopping, Participant, Good


class UserSerializator(serializers.ModelSerializer):
    class Meta:

        model = User
        fields = ('id', 'username', 'first_name', 'last_name')


class ParticipantCreateSerializer(serializers.ModelSerializer):
    class Meta:

        model = Participant
        fields = ('name', 'phone', 'ip',)

class GoodsSerializer(serializers.ModelSerializer):
    class Meta:

        model = Good
        fields = ('id', 'description', 'href', 'price', 'quantity', 'color', 'size', 'articul')

class ParticipantPublicSerializer(serializers.ModelSerializer):
    class Meta:

        model = Participant
        fields = ('name', )

class ParticipantPrivateSerializer(serializers.ModelSerializer):
    goods = GoodsSerializer(read_only=True, many=True)

    class Meta:
        model = Participant
        fields = ('name', 'phone', 'goods')

class ParticipantGoodsSerializer(serializers.ModelSerializer):
    goods = GoodsSerializer(read_only=True, many=True)

    class Meta:
        model = Participant
        fields = ('name', 'phone', 'goods')



class ShoppingSerializer(serializers.ModelSerializer):
    """ Public Shopping serializer """
    manager = UserSerializator(read_only=True)
    class Meta:

        model = Shopping
        fields = ('id', 'manager', 'title', 'description', 'date',
                  'conditions', 'delivery', 'date_close', 'is_close')
        read_only = ('date',)

    # def get_participant(self, obj):
    #     participant = getattr(obj.participant, 'participant', None)
    #     if participant:
    #         serializer = ParticipantCreateSerializer(participant)
    #         return serializer.data
    #     return None

class ShoppingSerializerId(serializers.ModelSerializer):
    """ Public Shopping serializer """
    manager = UserSerializator(read_only=True)
    participants = ParticipantPrivateSerializer(many=True)
    class Meta:

        model = Shopping
        fields = ('id', 'manager', 'title', 'description', 'date',
                  'conditions', 'delivery', 'participants', 'date_close', 'is_close')
        read_only = ('date', 'participants',)

    def validate_title(self, value):
        if len(value)<3:
            raise serializers.ValidationError("Blog post too short")
        return value