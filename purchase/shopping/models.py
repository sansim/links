from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.

class Good(models.Model):

    href = models.CharField('Ссылка на товар', max_length=255)
    color = models.CharField('Цвет', max_length=50, blank=True)
    size = models.CharField('Размер', max_length=50, blank=True)
    articul = models.CharField('Артикул', max_length=50, blank=True)
    quantity = models.PositiveIntegerField('Кол-во', default=1)
    description = models.TextField('Описание', blank=True)
    price = models.PositiveIntegerField('Цена', default=0)

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    def __str__(self):
        return self.href[:10]


class Participant(models.Model):
    token = models.CharField('token', max_length=50)
    name = models.CharField('ФИО', max_length=50)
    phone = models.CharField('Телефон', max_length=50, blank=True)
    contacts = models.TextField('Дополнительные контакты',  blank=True)
    goods = models.ManyToManyField(
        Good, blank=True, related_name='participant')
    ip = models.GenericIPAddressField('ip')
    user = models.ForeignKey(User, on_delete=models.SET_NULL,
                             verbose_name='Профиль пользователя', blank=True, null=True)

    class Meta:
        verbose_name = 'Участник'
        verbose_name_plural = 'Участники'

    def __str__(self):
        return self.name


class Shopping(models.Model):
    manager = models.ForeignKey(User, on_delete=models.CASCADE)
    participants = models.ManyToManyField(Participant, blank=True,)
    title = models.CharField('Название', max_length=50)
    description = models.TextField('Описание закупки', blank=True)
    date = models.DateTimeField('Дата', default=timezone.now)
    conditions = models.TextField('Условия закупки', blank=True)
    delivery = models.TextField('Доставка', blank=True)
    date_close = models.DateTimeField(
        'Дата завершения приема заявок', null=True, blank=True)
    is_close = models.BooleanField('Прием заявок закрыт', default=False)

    class Meta:
        verbose_name = 'Закупка'
        verbose_name_plural = 'Закупки'

    def is_closed(self):
        return self.is_close or (self.date_close is not None and
                                 self.date_close < timezone.now())

    def __str__(self):
        return "{0} {1}".format(self.date, self.title)
